<html>
<head>
    <title>Les clients</title>
    <link rel="stylesheet" href="../css/bootstrap.css" >
</head>
<body>
<h1 style="text-align: center">Liste des clients </h1>
<p/>
<hr width="100%"/>
<p><a href="/index.jsp">Liste des clients</a></p>
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        <th scope="col">Action</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <#list clients as client>
    <thead >
        <tr >
            <td > ${client.nom} </td>
            <td> ${client.pnom} </td>
            <td><a style="color: #1c7430" href="\update?id=${client.num}">MAJ</a></td>
            <td ><a style="color: red" href="\supprimer?id=${client.num}">Supprimer</a></td>
        </tr>
    </thead>
    </#list>
</table>


</body>
</html>