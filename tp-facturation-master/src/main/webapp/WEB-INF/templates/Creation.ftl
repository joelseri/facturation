<#--Permet à Freemarker de ne pas renvoyer d'erreur en cas de valeurs null-->
<#setting classic_compatible=true>

<html>
<head>
    <title>Création d'un nouvel clientt</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
</head>
<body>
<h1>Créer un nouveau client : </h1>
<p/>


<form method="post" action="/create" class="col-md-6">
    <div class="form-group row">
        <label for="text" class="col-md-3 col-form-label">#ID :</label>
        <div class="col-md-9">
            <input class="form-control" type="text" id="id" name="id" value="${client.num}"/>
        </div>
    </div>
    <div class="form-group row">
        <label for="nom" class="col-md-3 col-form-label">NOM :</label>
        <div class="col-md-9">
            <div>
                <input class="form-control" type="text" id="nom" name="nom" value="${client.nom}"/>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="pnom" class="col-md-3 col-form-label">Prénom :</label>
        <div class="col-md-9">
            <input class="form-control" type="text" id="pnom" name="pnom" value="${client.pnom}"/>
        </div>
    </div>
    <div class="form-group row">
        <label for="loc" class="col-md-3 col-form-label">Ville :</label>
        <div class="col-md-9">
            <input class="form-control" type="text" id="loc" name="loc" value="${client.loc}"/>
        </div>
    </div>
    <div class="form-group row">
        <label for="pays" class="col-md-3 col-form-label">Pays :</label>
        <div class="col-md-9">
            <input class="form-control" type="text" id="pays" name="pays" value="${client.pays}"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col">
            <button name=create" type="submit" class="btn btn-primary">Envoyer</button>
        </div>
    </div>
</form>

</body>
</html>