<html>
<head>
    <title>Delete client</title>
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
</head>
<body>
<h1>Effacer le client de la base de données ?</h1>
<p/>
<hr width="100%"/>

<form method="post" action="/supprimer">
    <fieldset>
        <legend>Client</legend>
        <div>
            <label for="id">ID : </label>
            <input type="text" id="id" name="id" value="${client.num}" readonly/>
        </div>
        <br/>
        <div>
            <label for="nom">Nom : </label>
            <input type="text" id="nom" name="nom" value="${client.nom}" readonly/>
        </div>
        <br/>
        <div>
            <label for="pnom">Prénom : </label>
            <input type="text" id="pnom" name="pnom" value="${client.pnom}" readonly/>
        </div>
        <br/>
        <div>
            <label for="loc">Ville : </label>
            <input type="text" id="loc" name="loc" value="${client.loc}" readonly/>
        </div>
        <br/>
        <div>
            <label for="pays">Pays : </label>
            <input type="text" id="pays" name="pays" value="${client.pays}" readonly/>
        </div>
        <br/>
        <br/>
        <div class="button">
            <button type="submit" name=delete">Supprimer le client dans la base de donnée</button>
        </div>
    </fieldset>
</form>

</body>
</html>