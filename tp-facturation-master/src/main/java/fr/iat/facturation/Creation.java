package fr.iat.facturation;

import fr.iat.facturation.model.Client;
import fr.iat.facturation.service.Database;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class Creation extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {

        Database db = (Database) getServletContext().getAttribute("db");
//        Connection conn = db.getConnection();

        String id = httpServletRequest.getParameter("id");
        String nom = httpServletRequest.getParameter("nom");
        String prenom = httpServletRequest.getParameter("pnom");
        String loc = httpServletRequest.getParameter("loc");
        String pays = httpServletRequest.getParameter("pays");

        try {

//            Statement statement = conn.createStatement();
            // INSERT --------------------------------------------------------------------------------------------------
//            String insertQuery = "INSERT INTO clients (clt_num, clt_nom, clt_pnom, clt_loc, clt_pays) VALUES ('" + id + "','" + nom + "','" + prenom + "','" + loc + "','" + pays + "')";
//            statement.executeUpdate(insertQuery);
            PreparedStatement statement = db.createClient(id, nom, prenom, loc, pays);
            statement.executeUpdate();

            // redirection
            httpServletResponse.sendRedirect("/clients");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {

        Database db = (Database) getServletContext().getAttribute("db");
//        Connection conn = db.getConnection();
        Template create = (Template) getServletContext().getAttribute("create");

        String id = httpServletRequest.getParameter("id");

        try {

//            Statement statement = conn.createStatement();

            // SELECT --------------------------------------------------------------------------------------------------
//            String query = "SELECT clt_num, clt_nom, clt_pnom, clt_loc, clt_pays FROM clients WHERE clt_num = '" + id + "'";
//            ResultSet rs = statement.executeQuery(query);
            PreparedStatement statement = db.selectAllFromClientsByNum(id);
            ResultSet rs = statement.executeQuery();
            Client client = null;
            while (rs.next()) {
                client = new Client(rs.getString("clt_num"),
                        rs.getString("clt_nom"),
                        rs.getString("clt_pnom"),
                        rs.getString("clt_loc"),
                        rs.getString("clt_pays"));
            }

            Map<String, Object> datas = new HashMap<>();
            datas.put("client", client);
            create.process(datas, httpServletResponse.getWriter());

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

}
