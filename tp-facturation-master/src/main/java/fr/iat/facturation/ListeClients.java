package fr.iat.facturation;

import fr.iat.facturation.model.Client;
import fr.iat.facturation.service.Database;
import fr.iat.facturation.service.ServletListener;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListeClients extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {

        Database db = (Database) getServletContext().getAttribute("db");
//        Connection conn = db.getConnection();
        Template listeClients = (Template) getServletContext().getAttribute("listeClients");

        try {

            // SELECT --------------------------------------------------------------------------------------------------
//            Statement statement = conn.createStatement();
//            String query = "SELECT clt_num, clt_nom, clt_pnom, clt_loc, clt_pays FROM clients";
//            ResultSet res = statement.executeQuery(query);
            PreparedStatement statement = db.selectAllFromClients();
            ResultSet res = statement.executeQuery();
            List<Client> clients = new ArrayList<>();
            while (res.next()) {
                clients.add(new Client(res.getString("clt_num"),
                        res.getString("clt_nom"),
                        res.getString("clt_pnom"),
                        res.getString("clt_loc"),
                        res.getString("clt_pays")));
            }

            Map<String, Object> datas = new HashMap<>();
            datas.put("clients", clients);
            listeClients.process(datas, httpServletResponse.getWriter());


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }


}
